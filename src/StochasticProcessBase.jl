module StochasticProcessBase

"`Core` of `StochasticProcessBase` describing type hierarchy."
module Core

	"Abstract type `StochasticProcess` for dispatch."
	abstract type StochasticProcess end

end

"`Interface` of `StochasticProcess` for function definitions."
module Interface

	"Compute `𝔼[ f(x) | 𝒟(x,y) ]` of a `StochasticProcess`."
	function expected end

	"Compute `var( f(x) | 𝒟(x,y) ) of a `StochasticProcess`."
	function variance end

	"Compute `std( f(x) | 𝒟(x,y) ) of a `StochasticProcess`."
	function standard end

	using SpecialFunctions: erfinv

	"Compute standard deviation within a credible or confidence interval provided as a real number `0 ≤ P < 1`."
	standard(args...; interval, kwargs...) =
		_standard(standard(args...; kwargs...), interval)

	_standard(σ::Number, P) = √2*erfinv(P) * σ

	_standard(σ::AbstractArray, P) = broadcast!(*, σ, √2*erfinv(P), σ)

end

end # StochasticProcessBase